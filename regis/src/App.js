import './App.css';
import { Steps, Input, Select, DatePicker, Row, Col, Divider, Radio } from 'antd';
import type { RadioChangeEvent } from 'antd';
import type { DatePickerProps, RangePickerProps } from 'antd/es/date-picker';
import { EditFilled } from '@ant-design/icons';
import 'antd/dist/antd.css';
import React, { useState } from 'react';

function App() {

  const { Step } = Steps;
  const { Option } = Select;
  const [current, setCurrent] = useState(0);
  const [register, setRegister] = useState("register");
  const [detail, setDetail] = useState("detail closed");
  const [title, setTitle] = useState("");
  const [buttonCheck, setButtonCheck] = useState("");
  const [bib, setBib] = useState("");
  const [titleTh, setTitleTh] = useState("");
  const [nameTh, setNameTh] = useState("");
  const [surnameTh, setSurnameTh] = useState("");
  const [titleEng, setTitleEng] = useState("");
  const [nameEng, setNameEng] = useState("");
  const [surnameEng, setSurnameEng] = useState("");
  const [date, setDate] = useState("");
  const [month, setMonth] = useState("");
  const [year, setYear] = useState("");
  const [dateMonthYear, setDateMonthYear] = useState("");
  const [email, setEmail] = useState("");
  const [idenNo, setIdenNo] = useState("");
  const [race, setRace] = useState("เคย");
  const [race1, setRace1] = useState("choose-box check");
  const [race2, setRace2] = useState("choose-box");
  const [hour, setHour] = useState("");
  const [minute, setMinute] = useState("");
  const [hourText, setHourText] = useState("");
  const [minuteText, setMinuteText] = useState("");
  const [conName, setConName] = useState("");
  const [conSurname, setConSurname] = useState("");
  const [conRelation, setConRelation] = useState("");
  const [conPhoneNo, setConPhoneNo] = useState("");
  const [conName2, setConName2] = useState("");
  const [conSurname2, setConSurname2] = useState("");
  const [conRelation2, setConRelation2] = useState("");
  const [conPhoneNo2, setConPhoneNo2] = useState("");
  const [bloodType, setBloodtype] = useState("A");
  const [bloodTypeA, setBloodtypeA] = useState("blood-box check");
  const [bloodTypeB, setBloodtypeB] = useState("blood-box");
  const [bloodTypeO, setBloodtypeO] = useState("blood-box");
  const [bloodTypeAB, setBloodtypeAB] = useState("blood-box");
  const [drug, setDrug] = useState("");
  const [drugMore, setDrugMore] = useState("");
  const [drugMoreText, setDrugMoreText] = useState("");
  const [disease, setDisease] = useState("");
  const [diseaseMore, setDiseaseMore] = useState("");
  const [diseaseMoreText, setDiseaseMoreText] = useState("");
  const [surgery, setSurgery] = useState("");
  const [surgeryMore, setSurgeryMore] = useState("");
  const [surgeryMoreText, setSurgeryMoreText] = useState("");
  const [surgeryYear, setSurgeryYear] = useState("");
  const [surgeryYearText, setSurgeryYearText] = useState("");
  const [child, setChild] = useState("");
  const [drugAlways, setDrugAlways] = useState("");
  const [drugAlwaysMore, setDrugAlwaysMore] = useState("");
  const [drugAlwaysMoreText, setDrugAlwaysMoreText] = useState("");
  const [sick, setSick] = useState("");
  const [workout, setWorkout] = useState("");
  const [abnormal, setAbnormal] = useState("");

  const steps = [
    {
      title: 'ข้อมูลส่วนตัว',
    },
    {
      title: 'ข้อมูลเกี่ยวกับการวิ่ง',
    },
    {
      title: 'ผู้ติดต่อฉุกเฉิน',
    },
    {
      title: 'ข้อมูลทางการแพทย์',
    },
  ];

  const next = () => {
    setCurrent(current + 1);
  };

  const prev = () => {
    setCurrent(current - 1);
  };

  const onChangeTitleTh = (value) => {
    setTitleTh(value);
  };

  const onChangeTitleEng = (value) => {
    setTitleEng(value);
  };

  const onChangeRadio1 = (e: RadioChangeEvent) => {
    setDrug(e.target.value);
  };

  const onChangeRadio2 = (e: RadioChangeEvent) => {
    setDisease(e.target.value);
  };

  const onChangeRadio3 = (e: RadioChangeEvent) => {
    setSurgery(e.target.value);
  };

  const onChangeRadio4 = (e: RadioChangeEvent) => {
    setChild(e.target.value);
  };

  const onChangeRadio5 = (e: RadioChangeEvent) => {
    setDrugAlways(e.target.value);
  };

  const onChangeRadio6 = (e: RadioChangeEvent) => {
    setSick(e.target.value);
  };

  const onChangeRadio7 = (e: RadioChangeEvent) => {
    setWorkout(e.target.value);
  };

  const onChangeRadio8 = (e: RadioChangeEvent) => {
    setAbnormal(e.target.value);
  };

  const onChangeDate = (value: DatePickerProps['value']) => {
    setDateMonthYear(value);
    setDate(value.date().toString());
    if (value.month() + 1 === 1)
      setMonth("มกราคม");
    else if (value.month() + 1 === 2)
      setMonth("กุมภาพันธ์");
    else if (value.month() + 1 === 3)
      setMonth("มีนาคม");
    else if (value.month() + 1 === 4)
      setMonth("เมษายน");
    else if (value.month() + 1 === 5)
      setMonth("พฤษภาคม");
    else if (value.month() + 1 === 6)
      setMonth("มิถุนายน");
    else if (value.month() + 1 === 7)
      setMonth("กรกฎาคม");
    else if (value.month() + 1 === 8)
      setMonth("สิงหาคม");
    else if (value.month() + 1 === 9)
      setMonth("กันยายน");
    else if (value.month() + 1 === 10)
      setMonth("คุลาคม");
    else if (value.month() + 1 === 11)
      setMonth("พฤศจิกายน");
    else if (value.month() + 1 === 12)
      setMonth("ธันวาคม");
    setYear((value.year() + 543).toString());
  }

  let uploadButton = document.getElementById("upload-button");
  let chosenImage = document.getElementById("chosen-image");
  let filename = document.getElementById("file-name");

  const uploadImage = () => {
    let reader = new FileReader();
    reader.readAsDataURL(uploadButton.files[0]);
    console.log(uploadButton.files[0]);
    reader.onload = () => {
      chosenImage.setAttribute("src",reader.result);
    }
  }

  return (
    <>
      <div class={register}>
        <div class="head">
          <span>ระบบลงทะเบียน</span>
        </div>
        <div class="content">
          <div class="text-title">
            <span>{"Part " + (current + 1) + " : " + steps[current].title}</span>
          </div>
          <Steps current={current}>
            {steps.map(item => (
              <Step key={item.title} title={item.title} />
            ))}
          </Steps>
          <div className="steps-content">
            {current === 0 && (
              <>
                {/* <Row>
                  <Col span={24}>
                    <button class="upload-img">
                      <span class="edit-icon"><EditFilled /></span>แก้ไขรูป
                    </button>
                    <input type="file" class="upload-img"></input>
                  </Col>
                </Row>
                <Row>
                  <Col span={24}>
                    <span class="descript">รูปถ่ายหน้าตรง 300x300 pixel</span>
                  </Col>
                </Row> */}
                <Row gutter={[16, 24]}>
                  <Col span={7}></Col>
                  <Col span={10}>
                    <div class="middle">
                      <input type="file" id="upload-button" accept="image/*" onChange={uploadImage}></input>
                      <label for="upload-button" class="upload-file">
                        <Row>
                            <Col span={24}>
                        <span class="edit-icon"><EditFilled /></span>
                        </Col>
                            <Col span={24}>
                        แก้ไขรูป
                        </Col>
                          </Row>
                      </label>
                    </div>
                    <div class="middle">
                      <span class="descript">รูปถ่ายหน้าตรง 300x300 pixel</span>
                    </div>
                  </Col>
                  <Col span={7}></Col>
                  <Col span={7}></Col>
                  <Col span={10}>
                    <Row>
                      <Col span={8}>
                        <span>ชื่อบนเบอร์วิ่ง (BIB)</span>
                      </Col>
                      <Col span={16}>
                        <Input value={bib} onChange={(e) => { setBib(e.target.value) }} required/>
                      </Col>
                      <span class="descript">กรอกได้สูงสุด 10 ตัวอักษร ภาษาอังกฤษตัวพิมพ์ใหญ่ และตัวเลขเท่านั้น</span>
                    </Row>
                  </Col>
                  <Col span={7}></Col>
                  <Col span={8}>
                    <Row>
                      <Col span={8}>
                        <span>คำนำหน้าชื่อ</span><span class="star">*</span>
                      </Col>
                      <Col span={16}>
                        <Select
                          placeholder=""
                          onChange={onChangeTitleTh}
                          allowClear
                          style={{ width: 150 }}
                          value={titleTh}
                          required
                        >
                          <Option value="นาย">นาย</Option>
                          <Option value="นาง">นาง</Option>
                          <Option value="นางสาว">นางสาว</Option>
                        </Select>
                      </Col>
                    </Row>
                  </Col>
                  <Col span={8}>
                    <Row>
                      <Col span={8}>
                        <span>ชื่อ</span><span class="star">*</span>
                      </Col>
                      <Col span={16}>
                        <Input value={nameTh} onChange={(e) => { setNameTh(e.target.value) }} />
                      </Col>
                    </Row>
                  </Col>
                  <Col span={8}>
                    <Row>
                      <Col span={8}>
                        <span>นามสกุล</span><span class="star">*</span>
                      </Col>
                      <Col span={16}>
                        <Input value={surnameTh} onChange={(e) => { setSurnameTh(e.target.value) }} />
                      </Col>
                    </Row>
                  </Col>

                  <Col span={8}>
                    <Row>
                      <Col span={8}>
                        <span>Title</span><span class="star">*</span>
                      </Col>
                      <Col span={16}>
                        <Select
                          placeholder=""
                          onChange={onChangeTitleEng}
                          allowClear
                          style={{ width: 150 }}
                          value={titleEng}
                        >
                          <Option value="Mr.">Mr.</Option>
                          <Option value="Mrs.">Mrs.</Option>
                          <Option value="Ms.">Ms.</Option>
                        </Select>
                      </Col>
                    </Row>
                  </Col>
                  <Col span={8}>
                    <Row>
                      <Col span={8}>
                        <span>Name</span><span class="star">*</span>
                      </Col>
                      <Col span={16}>
                        <Input value={nameEng} onChange={(e) => { setNameEng(e.target.value) }} />
                      </Col>
                    </Row>
                  </Col>
                  <Col span={8}>
                    <Row>
                      <Col span={8}>
                        <span>Surname</span><span class="star">*</span>
                      </Col>
                      <Col span={16}>
                        <Input value={surnameEng} onChange={(e) => { setSurnameEng(e.target.value) }} />
                      </Col>
                    </Row>
                  </Col>

                  <Col span={8}>
                    <Row>
                      <Col span={8}>
                        <span>วันเดือนปีเกิด</span><span class="star">*</span>
                      </Col>
                      <Col span={16}>
                        <DatePicker value={dateMonthYear} onChange={onChangeDate}
                          style={{ width: 215 }} />
                      </Col>
                    </Row>
                  </Col>
                  <Col span={8}>
                    <Row>
                      <Col span={8}>
                        <span>อีเมล</span><span class="star">*</span>
                      </Col>
                      <Col span={16}>
                        <Input value={email} onChange={(e) => { setEmail(e.target.value) }} />
                      </Col>
                    </Row>
                  </Col>
                  <Col span={8}>
                    <Row>
                      <Col span={8}>
                        <span>เลขบัตรประชาชน</span><span class="star">*</span>
                      </Col>
                      <Col span={16}>
                        <Input value={idenNo} onChange={(e) => { setIdenNo(e.target.value) }} />
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </>
            )}
            {current === 1 && (
              <>
                <div class="middle">
                  <Row gutter={[16, 16]}>
                    <Col span={7}></Col>
                    <Col span={10}>
                      <span class="ques1">เคยลงงานแข่ง Mini Marathon / Half Marathon / Full Marathon มาก่อนหรือไม่</span>
                    </Col>
                    <Col span={7}></Col>
                    <Col span={7}></Col>
                    <Col span={10}>
                      <button class={race1} onClick={() => {
                        setRace("เคย")
                        setRace1("choose-box check")
                        setRace2("choose-box")
                      }}>เคย
                      </button>
                      <button class={race2} onClick={() => {
                        setRace("ไม่เคย")
                        setRace1("choose-box")
                        setRace2("choose-box check")
                      }}>ไม่เคย
                      </button>
                    </Col>
                    <Col span={7}></Col>
                    <Col span={7}></Col>
                    <Col span={10}>
                      <span>เวลาที่คาดว่าจะจบในการวิ่งครั้งนี้ (ชั่วโมงและนาที)</span>
                    </Col>
                    <Col span={7}></Col>
                    <Col span={7}></Col>
                    <Col span={10}>
                      <input class="input" value={hour} onChange={(e) => {
                        setHour(e.target.value)
                        setHourText(e.target.value + " ชั่วโมง")
                      }}></input>
                      <span> : </span>
                      <input class="input" value={minute} onChange={(e) => {
                        setMinute(e.target.value)
                        setMinuteText(e.target.value + " นาที")
                      }}></input>
                    </Col>
                    <Col span={7}></Col>
                  </Row>
                </div>
              </>
            )}
            {current === 2 && (
              <>
                <Row gutter={[36, 24]}>
                  <Col span={24}>
                    <div class="middle">
                      <span class="red-head">ผู้ติดต่อฉุกเฉินคนที่ 1*</span>
                    </div>
                  </Col>
                  <Col span={4}></Col>
                  <Col span={8}>
                    <Row>
                      <Col span={8}>
                        <span>ชื่อ</span><span class="star">*</span>
                      </Col>
                      <Col span={16}>
                        <Input value={conName} onChange={(e) => { setConName(e.target.value) }} />
                      </Col>
                    </Row>
                  </Col>
                  <Col span={8}>
                    <Row>
                      <Col span={8}>
                        <span>นามสกุล</span><span class="star">*</span>
                      </Col>
                      <Col span={16}>
                        <Input value={conSurname} onChange={(e) => { setConSurname(e.target.value) }} />
                      </Col>
                    </Row>
                  </Col>
                  <Col span={4}></Col>
                  <Col span={4}></Col>
                  <Col span={8}>
                    <Row>
                      <Col span={8}>
                        <span>ความสัมพันธ์</span><span class="star">*</span>
                      </Col>
                      <Col span={16}>
                        <Input value={conRelation} onChange={(e) => { setConRelation(e.target.value) }} />
                      </Col>
                    </Row>
                  </Col>
                  <Col span={8}>
                    <Row>
                      <Col span={8}>
                        <span>เบอร์โทรศัพท์</span><span class="star">*</span>
                      </Col>
                      <Col span={16}>
                        <Input value={conPhoneNo} onChange={(e) => { setConPhoneNo(e.target.value) }} />
                      </Col>
                    </Row>
                  </Col>
                  <Col span={4}></Col>
                </Row>
                <Divider />
                <Row gutter={[36, 24]}>
                  <Col span={24}>
                    <div class="middle">
                      <span class="red-head">ผู้ติดต่อฉุกเฉินคนที่ 2 (ถ้ามี)</span>
                    </div>
                  </Col>
                  <Col span={4}></Col>
                  <Col span={8}>
                    <Row>
                      <Col span={8}>
                        <span>ชื่อ</span>
                      </Col>
                      <Col span={16}>
                        <Input value={conName2} onChange={(e) => { setConName2(e.target.value) }} />
                      </Col>
                    </Row>
                  </Col>
                  <Col span={8}>
                    <Row>
                      <Col span={8}>
                        <span>นามสกุล</span>
                      </Col>
                      <Col span={16}>
                        <Input value={conSurname2} onChange={(e) => { setConSurname2(e.target.value) }} />
                      </Col>
                    </Row>
                  </Col>
                  <Col span={4}></Col>
                  <Col span={4}></Col>
                  <Col span={8}>
                    <Row>
                      <Col span={8}>
                        <span>ความสัมพันธ์</span>
                      </Col>
                      <Col span={16}>
                        <Input value={conRelation2} onChange={(e) => { setConRelation2(e.target.value) }} />
                      </Col>
                    </Row>
                  </Col>
                  <Col span={8}>
                    <Row>
                      <Col span={8}>
                        <span>เบอร์โทรศัพท์</span>
                      </Col>
                      <Col span={16}>
                        <Input value={conPhoneNo2} onChange={(e) => { setConPhoneNo2(e.target.value) }} />
                      </Col>
                    </Row>
                  </Col>
                  <Col span={4}></Col>
                </Row>
              </>
            )}
            {current === 3 && (
              <>
                <Row gutter={[36, 16]}>
                  <Col span={10}>
                    <div class="blood-right">
                      <span>หมู่เลือด</span>
                    </div>
                  </Col>
                  <Col span={14}>
                    <button class={bloodTypeA} onClick={() => {
                      setBloodtype("A")
                      setBloodtypeA("blood-box check")
                      setBloodtypeB("blood-box")
                      setBloodtypeO("blood-box")
                      setBloodtypeAB("blood-box")
                    }}>A
                    </button>
                    <button class={bloodTypeB} onClick={() => {
                      setBloodtype("B")
                      setBloodtypeA("blood-box")
                      setBloodtypeB("blood-box check")
                      setBloodtypeO("blood-box")
                      setBloodtypeAB("blood-box")
                    }}>B
                    </button>
                    <button class={bloodTypeO} onClick={() => {
                      setBloodtype("O")
                      setBloodtypeA("blood-box")
                      setBloodtypeB("blood-box")
                      setBloodtypeO("blood-box check")
                      setBloodtypeAB("blood-box")
                    }}>O
                    </button>
                    <button class={bloodTypeAB} onClick={() => {
                      setBloodtype("AB")
                      setBloodtypeA("blood-box")
                      setBloodtypeB("blood-box")
                      setBloodtypeO("blood-box")
                      setBloodtypeAB("blood-box check")
                    }}>AB
                    </button>
                  </Col>
                  <Col span={10}>
                    <div class="right">
                      <span>ท่านมีอาการแพ้ยาหรือสารต่างๆหรือไม่</span><span class="star">*</span>
                    </div>
                  </Col>
                  <Col span={12}>
                    <Row>
                      <Col span={8}>
                        <Radio.Group onChange={onChangeRadio1} value={drug}>
                          <Radio value={"ไม่มี"}>ไม่มี</Radio>
                          <Radio value={"มี"}>มี</Radio>
                        </Radio.Group>
                      </Col>
                      <Col span={16}>
                        <Row>
                          <Col span={8}>
                            <span>ระบุเพิ่มเติม</span>
                          </Col>
                          <Col span={16}>
                            <Input value={drugMoreText} onChange={(e) => {
                              setDrugMore("(ยา " + e.target.value + ")")
                              setDrugMoreText(e.target.value)
                            }} />
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                  </Col>
                  <Col span={2}></Col>
                  <Col span={10}>
                    <div class="right">
                      <span>ท่านมีโรคประจำตัวหรือไม่</span><span class="star">*</span>
                    </div>
                  </Col>
                  <Col span={12}>
                    <Row>
                      <Col span={8}>
                        <Radio.Group onChange={onChangeRadio2} value={disease}>
                          <Radio value={"ไม่มี"}>ไม่มี</Radio>
                          <Radio value={"มี"}>มี</Radio>
                        </Radio.Group>
                      </Col>
                      <Col span={16}>
                        <Row>
                          <Col span={8}>
                            <span>ระบุเพิ่มเติม</span>
                          </Col>
                          <Col span={16}>
                            <Input value={diseaseMoreText} onChange={(e) => {
                              setDiseaseMore("(โรค " + e.target.value + ")")
                              setDiseaseMoreText(e.target.value)
                            }} />
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                  </Col>
                  <Col span={2}></Col>
                  <Col span={10}>
                    <div class="right">
                      <span>ท่านมีประวัติการผ่าตัดมาก่อนหรือไม่</span><span class="star">*</span>
                    </div>
                  </Col>
                  <Col span={12}>
                    <Row>
                      <Col span={8}>
                        <Radio.Group onChange={onChangeRadio3} value={surgery}>
                          <Radio value={"ไม่มี"}>ไม่มี</Radio>
                          <Radio value={"มี"}>มี</Radio>
                        </Radio.Group>
                      </Col>
                      <Col span={16}>
                        <Row gutter={[0, 16]}>
                          <Col span={8}>
                            <span>ตำแหน่ง</span>
                          </Col>
                          <Col span={16}>
                            <Input value={surgeryMoreText} onChange={(e) => {
                              setSurgeryMore("ตำแหน่ง : " + e.target.value)
                              setSurgeryMoreText(e.target.value)
                            }} />
                          </Col>
                          <Col span={8}>
                            <span>ปีที่ผ่าตัด</span>
                          </Col>
                          <Col span={16}>
                            <Input value={surgeryYearText} onChange={(e) => {
                              setSurgeryYear("ปี : " + e.target.value)
                              setSurgeryYearText(e.target.value)
                            }} style={{ width: 101 }} />
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                  </Col>
                  <Col span={2}></Col>
                  <Col span={10}>
                    <div class="right">
                      <span>ท่านมีแพลนที่จะมีบุตรหรือกำลังตั้งครรภ์ก่อนถึงช่วงการแข่งขันหรือไม่</span><span class="star">*</span>
                    </div>
                  </Col>
                  <Col span={12}>
                    <Radio.Group onChange={onChangeRadio4} value={child}>
                      <Radio value={"ไม่มี"}>ไม่มี</Radio>
                      <Radio value={"มี"}>มี</Radio>
                    </Radio.Group>
                  </Col>
                  <Col span={2}></Col>
                  <Col span={10}>
                    <div class="right">
                      <span>ท่านมียาที่ต้องทานเป็นประจำหรือไม่</span><span class="star">*</span>
                    </div>
                  </Col>
                  <Col span={12}>
                    <Row>
                      <Col span={8}>
                        <Radio.Group onChange={onChangeRadio5} value={drugAlways}>
                          <Radio value={"ไม่มี"}>ไม่มี</Radio>
                          <Radio value={"มี"}>มี</Radio>
                        </Radio.Group>
                      </Col>
                      <Col span={16}>
                        <Row>
                          <Col span={8}>
                            <span>ระบุเพิ่มเติม</span>
                          </Col>
                          <Col span={16}>
                            <Input value={drugAlwaysMoreText} onChange={(e) => {
                              setDrugAlwaysMore("(ยา " + e.target.value + ")")
                              setDrugAlwaysMoreText(e.target.value)
                            }} />
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                  </Col>
                  <Col span={2}></Col>
                  <Col span={24}>
                    <div class="middle">
                      <span>ท่านเคยบาดเจ็บ/ เจ็บป่วย จากการเข้าร่วมงานวิ่งที่ต้องไปรักษาต่อที่โรงพยาบาลหรือไม่</span><span class="star">*</span>
                    </div>
                  </Col>
                  <Col span={10}></Col>
                  <Col span={12}>
                    <Row>
                      <Col span={8}>
                        <Radio.Group onChange={onChangeRadio6} value={sick}>
                          <Radio value={"เคย"}>เคย</Radio>
                          <Radio value={"ไม่เคย"}>ไม่เคย</Radio>
                        </Radio.Group>
                      </Col>
                      <Col span={16}></Col>
                    </Row>
                  </Col>
                  <Col span={2}></Col>
                  <Col span={24}>
                    <div class="middle">
                      <span>ท่านออกกำลังกายสม่ำเสมอหรือไม่ (อย่างน้อย 1-2 ครั้ง ต่อสัปดาห์)</span><span class="star">*</span>
                    </div>
                  </Col>
                  <Col span={10}></Col>
                  <Col span={12}>
                    <Row>
                      <Col span={8}>
                        <Radio.Group onChange={onChangeRadio7} value={workout}>
                          <Radio value={"ใช่"}>ใช่</Radio>
                          <Radio value={"ไม่ใช่"}>ไม่ใช่</Radio>
                        </Radio.Group>
                      </Col>
                      <Col span={16}></Col>
                    </Row>
                  </Col>
                  <Col span={2}></Col>
                  <Col span={24}>
                    <div class="middle">
                      <span>ท่านเคยมีอาการเจ็บแน่นหน้าอก ใจสั่น เหนื่อยง่ายผิดปกติ หน้ามืด ขณะออกกำลังกายหรือไม่?</span><span class="star">*</span>
                    </div>
                  </Col>
                  <Col span={10}></Col>
                  <Col span={12}>
                    <Row>
                      <Col span={8}>
                        <Radio.Group onChange={onChangeRadio8} value={abnormal}>
                          <Radio value={"เคย"}>เคย</Radio>
                          <Radio value={"ไม่เคย"}>ไม่เคย</Radio>
                        </Radio.Group>
                      </Col>
                      <Col span={16}></Col>
                    </Row>
                  </Col>
                  <Col span={2}></Col>
                </Row>
              </>
            )}
          </div>
          <div className="steps-action">
            {current > 0 && (
              <button class="btn1" onClick={() => prev()}>
                ก่อนหน้า
              </button>
            )}
            {current <= steps.length - 1 && (
              <button class="btn2">
                Save Draft
              </button>
            )}
            {current < steps.length - 1 && (
              <button class="btn3" onClick={() => next()}>
                ถัดไป
              </button>
            )}
            {current === steps.length - 1 && (
              <button class="btn4" onClick={() => {
                setRegister("register closed")
                setDetail("detail")
              }}>
                ตรวจสอบข้อมูล
              </button>
            )}
          </div>
        </div>
      </div>
      {/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------ */}
      <div class={detail}>
        <div class="head">
          <span>ข้อมูลทั้งหมด</span>
        </div>
        <div class="right">
          <button class="edit-button" onClick={() => {
            setRegister("register")
            setDetail("detail closed")
          }}><EditFilled />
          </button>
        </div>
        <div class="detail-content">
          <Row gutter={[16, 16]}>
            <Col span={12}>
              <div class="content con1">
                <Row gutter={[16, 8]}>
                  <Col span={24}>
                    <span class="head-detail">ข้อมูลส่วนตัว</span>
                  </Col>
                  <Col span={24}>
                    <div class="middle">
                      <img id="chosen-image"></img>
                      <figcaption id="file-name"></figcaption>
                    </div>
                  </Col>
                  <Col span={24}>
                    <div class="middle">
                      <span>ชื่อบนเบอร์วิ่ง (BIB) :</span>
                    </div>
                  </Col>
                  <Col span={24}>
                    <div class="middle">
                      <span class="text-detail">{" " + bib}</span>
                    </div>
                  </Col>
                  <Col span={24}>
                    <span>Name-Surname :</span>
                  </Col>
                  <Col span={24}>
                    <span class="text-detail">{" " + titleEng + " " + nameEng + " " + surnameEng}</span>
                  </Col>
                  <Col span={24}>
                    <span>ชื่อ-นามสกุล :</span>
                  </Col>
                  <Col span={24}>
                    <span class="text-detail">{" " + titleTh + nameTh + " " + surnameTh}</span>
                  </Col>
                  <Col span={24}>
                    <span>วันเดือนปีเกิด :</span>
                  </Col>
                  <Col span={24}>
                    <span class="text-detail">{" " + date + " " + month + " " + year}</span>
                  </Col>
                  <Col span={24}>
                    <span>อีเมล :</span>
                  </Col>
                  <Col span={24}>
                    <span class="text-detail">{" " + email}</span>
                  </Col>
                  <Col span={24}>
                    <span>เลขบัตรประชาชน :</span>
                  </Col>
                  <Col span={24}>
                    <span class="text-detail">{" " + idenNo}</span>
                  </Col>
                </Row>
              </div>

              <div class="content con3">
                <Row gutter={[16, 8]}>
                  <Col span={24}>
                    <span class="head-detail">ผู้ติดต่อฉุกเฉิน</span>
                  </Col>
                  <Col span={24}>
                    <div class="middle">
                      <span class="red-detail">ผู้ติดต่อฉุกเฉินคนที่ 1</span>
                    </div>
                  </Col>
                  <Col span={24}>
                    <span>ชื่อ-นามสกุล :</span><span class="text-detail">{" " + conName + " " + conSurname}</span>
                  </Col>
                  <Col span={24}>
                    <Row gutter={[16, 0]}>
                      <Col span={12}>
                        <span>ความสัมพันธ์ :</span><span class="text-detail">{" " + conRelation}</span>
                      </Col>
                      <Col span={12}>
                        <span>เบอร์โทรศัพท์ :</span><span class="text-detail">{" " + conPhoneNo}</span>
                      </Col>
                    </Row>
                  </Col>
                  <Divider />
                  <Col span={24}>
                    <div class="middle">
                      <span class="red-detail">ผู้ติดต่อฉุกเฉินคนที่ 2</span>
                    </div>
                  </Col>
                  <Col span={24}>
                    <span>ชื่อ-นามสกุล :</span><span class="text-detail">{" " + conName2 + " " + conSurname2}</span>
                  </Col>
                  <Col span={24}>
                    <Row gutter={[16, 0]}>
                      <Col span={12}>
                        <span>ความสัมพันธ์ :</span><span class="text-detail">{" " + conRelation2}</span>
                      </Col>
                      <Col span={12}>
                        <span>เบอร์โทรศัพท์ :</span><span class="text-detail">{" " + conPhoneNo2}</span>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </div>
            </Col>
            <Col span={12}>
              <div class="content con2">
                <Row gutter={[16, 16]}>
                  <Col span={24}>
                    <span class="head-detail">ข้อมูลเกี่ยวกับการวิ่ง</span>
                  </Col>
                  <Col span={24}>
                    <span>เคยลงงานแข่ง Mini Marathon / Half Marathon / Full Marathon มาก่อนหรือไม่ :</span>
                  </Col>
                  <Col span={24}>
                    <span class="text-detail">{" " + race}</span>
                  </Col>
                  <Col span={24}>
                    <span>เวลาที่คาดว่าจะจบในการวิ่งครั้งนี้ (ชั่วโมงและนาที) :</span>
                  </Col>
                  <Col span={24}>
                    <span class="text-detail">{" " + hourText + " " + minuteText}</span>
                  </Col>
                </Row>
              </div>

              <div class="content con4">
                <Row gutter={[16, 4]}>
                  <Col span={24}>
                    <span class="head-detail">ข้อมูลทางการแพทย์</span>
                  </Col>
                  <Col span={24}>
                    <span>หมู่เลือด :</span><span class="text-detail">{" " + bloodType}</span>
                  </Col>
                  <Col span={24}>
                    <span>ท่านมีอาการแพ้ยาหรือสารต่างๆหรือไม่ :</span>
                  </Col>
                  <Col span={24}>
                    <span class="text-detail">{" " + drug}</span><span class="red-detail">{" " + drugMore}</span>
                  </Col>
                  <Col span={24}>
                    <span>ท่านมีโรคประจำตัวหรือไม่ :</span>
                  </Col>
                  <Col span={24}>
                    <span class="text-detail">{" " + disease}</span><span class="red-detail">{" " + diseaseMore}</span>
                  </Col>
                  <Col span={24}>
                    <span>ท่านมีประวัติการผ่าตัดมาก่อนหรือไม่ :</span>
                  </Col>
                  <Col span={24}>
                    <span class="text-detail">{" " + surgery}</span><span class="red-detail">{" " + surgeryMore + " " + surgeryYear}</span>
                  </Col>
                  <Col span={24}>
                    <span>ท่านมีแพลนที่จะมีบุตรหรือกำลังตั้งครรภ์ก่อนถึงช่วงการแข่งขันหรือไม่ :</span>
                  </Col>
                  <Col span={24}>
                    <span class="text-detail">{" " + child}</span>
                  </Col>
                  <Col span={24}>
                    <span>ท่านมียาที่ต้องทานเป็นประจำหรือไม่ :</span>
                  </Col>
                  <Col span={24}>
                    <span class="text-detail">{" " + drugAlways}</span><span class="red-detail">{" " + drugAlwaysMore}</span>
                  </Col>
                  <Col span={24}>
                    <span>ท่านเคยบาดเจ็บ/ เจ็บป่วย จากการเข้าร่วมงานวิ่งที่ต้องไปรักษาต่อที่โรงพยาบาลหรือไม่ :</span>
                  </Col>
                  <Col span={24}>
                    <span class="text-detail">{" " + sick}</span>
                  </Col>
                  <Col span={24}>
                    <span>ท่านออกกำลังกายสม่ำเสมอหรือไม่ (อย่างน้อย 1-2 ครั้ง ต่อสัปดาห์) :</span>
                  </Col>
                  <Col span={24}>
                    <span class="text-detail">{" " + workout}</span>
                  </Col>
                  <Col span={24}>
                    <span>ท่านเคยมีอาการเจ็บแน่นหน้าอก ใจสั่น เหนื่อยง่ายผิดปกติ หน้ามืด ขณะออกกำลังกายหรือไม่? :</span>
                  </Col>
                  <Col span={24}>
                    <span class="text-detail">{" " + abnormal}</span>
                  </Col>
                </Row>
              </div>
            </Col>
          </Row>
        </div>

        <div class="middle">
          <button class="btn2">
            ส่งข้อมูล
          </button>
        </div>

      </div>
    </>
  );
}

export default App;
